/*      Orage - Calendar and alarm handler
 *
 * Copyright (c) 2021 Erkki Moorits  (erkki.moorits at mail.ee)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
       Free Software Foundation
       51 Franklin Street, 5th Floor
       Boston, MA 02110-1301 USA

 */

#ifndef ORAGE_CSS_H
#define ORAGE_CSS_H 1

#define ORAGE_DAY_VIEW_SEPARATOR_BAR "orage_day_view_hour_line"
#define ORAGE_DAY_VIEW_OCCUPIED_HOUR_LINE "orage_day_view_occupied_hour_line"
#define ORAGE_DAY_VIEW_TASK_SEPARATOR "orage_day_view_task_separator"
#define ORAGE_DAY_VIEW_ODD_HOURS "orage_day_view_odd_hours"
#define ORAGE_DAY_VIEW_EVEN_HOURS "orage_day_view_even_hours"
#define ORAGE_DAY_VIEW_ALL_DAY_EVENT "orage_day_view_all_day_event"
#define ORAGE_DAY_VIEW_TODAY "orage_day_view_today"
#define ORAGE_DAY_VIEW_SUNDAY "orage_day_view_sunday"
#define ORAGE_MAINBOX_RED "orage_mainbox_red"
#define ORAGE_MAINBOX_BLUE "orage_mainbox_blue"

extern void register_css_provider (void);

#endif
